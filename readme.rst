=======================
Agenda de telefonos
======================


INSTALACIÓN
=============

1. Clonar el proyecto a aptana, dentro de la carpeta public_html
2. Dar permisos al proyecto $sudo chmod -R 777 /home/user/public_html/agendaphptarea
3. Ir la navegador y escrbir: localhost/~user
4. Seleccionar nuestro proyecto e ir a inicio.php 


USO
=====
La agenda se crea con cuatro contactos por defectos, podemos:
	- crear contactos
	- modificar contactos
	- eliminar uno o varios o todos los contactos
	- mostrar los contactos ordenados por diferentes opciones
	- buscar contactos por cualquiera de sus campos, mostrando los que contengan lo que se escriba en la búsqueda.
