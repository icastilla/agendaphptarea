<?php 

// funciones para la aplicacion


// funcion para crear la base de datos en sqlite con 4 contactos iniciales

function crearbd(){
	$crea_tabla = "CREATE TABLE IF NOT EXISTS contactos(
						id INTEGER PRIMARY KEY AUTOINCREMENT,
						nombre VARCHAR(40),
						apellidos VARCHAR(80),
						telefono VARCHAR(9),
						email VARCHAR(50)
						)";
						
	$contactos = array(
			array(
					'nombre'=>"aaa",
					'apellidos'=>"aaaa aa",
					'telefono'=>"111 11 11 11",
					'email'=>"aa@aa.com"
			),
			array(
					'nombre'=>"bbb",
					'apellidos'=>"bbbb bb",
					'telefono'=>"222 22 22 22",
					'email'=>"bb@bb.com"
			),
			array(
					'nombre'=>"ccc",
					'apellidos'=>"cccc cc",
					'telefono'=>"333 33 33 33",
					'email'=>"cc@cc.com"
			),
			array(
					'nombre'=>"ddd",
					'apellidos'=>"dddd ddd",
					'telefono'=>"444 44 44 44",
					'email'=>"dd@dd.com"
			)
	);
	$bd='agendaphp.sqlite';
		if(!file_exists($bd)){
			try{
				$conn = new PDO('sqlite:agendaphp.sqlite');
				$conn->exec($crea_tabla);
		
				$insertar = "INSERT INTO contactos(nombre, apellidos, telefono, email)
				VALUES(:nombre, :apellidos, :telefono, :email)";
				$sentencia = $conn->prepare($insertar);

				foreach($contactos as $contacto){
					$sentencia->execute($contacto);
				}
			}catch (PDOException $e){
				echo 'ERROR: ' . $e->getMessage();
			}
			$conn = null; 
		}
}
// funcion para buscar contacto
function buscar(){
	$consulta = "SELECT * FROM contactos WHERE ";
	
	if($_POST['nombre'] != ""){
		$consulta = $consulta . "nombre LIKE '%".$_POST['nombre']."%'";
		$and=true;
	}
	if($_POST['apellidos']!= ""){
		if ($and){
			$consulta = $consulta ." AND
			apellidos LIKE '%".$_POST['apellidos']."%'";
		}else{
			$consulta = $consulta ."
			apellidos LIKE '%".$_POST['apellidos']."%'";
			$and=true;
		}
	}
	if($_POST['email'] != ""){
		if ($and){
			$consulta = $consulta ." AND
			email LIKE '%".$_POST['email']."%'";
		}else{
			$consulta = $consulta ."
			email LIKE '%".$_POST['email']."%'";
			$and=true;
		}
	}
	if($_POST['telefono'] != ""){
		if ($and){
			$consulta = $consulta ." AND
			telefono LIKE '%".$_POST['telefono']."%'";
		}else{
			$consulta = $consulta ."
			telefono LIKE '%".$_POST['telefono']."%'";
			$and=true;
		}
	}
	
	try{
		$conn = new PDO('sqlite:agendaphp.sqlite');
		$resultado = $conn -> query($consulta);
	
		echo "<table class='contactos'>
		<th><tr><td>Nombre</td><td>Apellidos</td>
		<td>Telefono</td><td>Email</td></tr></th>";
		foreach ($resultado as $contacto) {
			echo "<tr><td>", $contacto['nombre'],"</td>";
			echo "<td>", $contacto['apellidos'],"</td>";
			echo "<td>", $contacto['telefono'],"</td>";
			echo "<td>", $contacto['email'],"</td></tr>";
		}
		echo "</table>";
		$conn = null;
	}catch(PDOException $e){
		echo $e -> getMessage();
	}
}


// funcion para crear un formulario de contacto

function crearform($accion, $contacto = ""){

	echo '	<form action="'. $accion .'" method="POST">
			<table><tr><td><label for="nombre">Nombre:</label></td>';
	echo	'		   <td><input type="text" id="nombre" name="nombre"';

	if($accion =='grabar.php'){
		echo "value ='" .$contacto['nombre']."'";
	}
	echo    '/></td>
	</tr>
	<tr>
	<td><label for="apellidos">apellidos:</label></td>';
	echo	'		<td><input type="text" id="apellidos" name="apellidos"';
	if($accion =='grabar.php'){
		echo "value ='" .$contacto['apellidos']."'";
	}
	echo    '/></td>
	</tr>
	<tr>
	<td><label for="email">email:</label></td>';
	echo	'		<td><input type="text" id="email" name="email"';
	if($accion =='grabar.php'){
		echo "value ='" .$contacto['email']."'";
	}
	echo    '/></td>
	</tr>
	<tr>
	<td><label for="telefono">telefono:</label></td>';
	echo	'		<td><input type="text" id="telefono" name="telefono"';
	if($accion =='grabar.php'){
		echo "value ='" .$contacto['telefono']."'";
	}
	echo    '/></td>
	</tr>';
	echo "<tr><td id='inv'><input  name='id' value='".$contacto['id']."' />".$contacto['id']."</td></tr>";
	

	
	if($accion =='buscar.php'){
		echo '<tr><td><input type="submit" value="BUSCAR" /></td></tr>';
	}else{
		echo '<tr><td><input type="submit" value="GUARDAR" /></td></tr>';
	}
	echo	'</table>';
	echo	'</form>';
}

// funcion para listar los contactos

function listar(){

	try{
		$conn = new PDO('sqlite:agendaphp.sqlite');
		$consulta = "SELECT * FROM contactos";
		$resultado = $conn -> query($consulta);

		echo "<table class='contactos'>
		<tr><th>Nombre<a href='orderNombre.php'><img class='flecha' src='img/flecha.jpg' alt='ordenar'><a href='orderNombre2.php'><img class='flecha' src='img/flecha2.jpg' alt='ordenar'></a></th>
		<th>Apellidos<a href='orderApe.php'><img class='flecha' src='img/flecha.jpg' alt='ordenar'></a><a href='orderApe2.php'><img class='flecha' src='img/flecha2.jpg' alt='ordenar'></th>
		<th>Telefono<a href='orderTelf.php'><img class='flecha' src='img/flecha.jpg' alt='ordenar'></a><a href='orderTelf2.php'><img class='flecha' src='img/flecha2.jpg' alt='ordenar'></th>
		<th>Email<a href='orderEmail.php'><img class='flecha' src='img/flecha.jpg' alt='ordenar'></a><a href='orderEmail2.php'><img class='flecha' src='img/flecha2.jpg' alt='ordenar'></th></tr>";
		foreach ($resultado as $contacto) {
			echo "<tr><td>", $contacto['nombre'],"</td>";
			echo "<td>", $contacto['apellidos'],"</td>";
			echo "<td>", $contacto['telefono'],"</td>";
			echo "<td>", $contacto['email'],"</td></tr>";
		}
		echo "</table>";
		
	}catch(PDOException $e ){
		echo $e -> getMessage();
	}
	$conn = null;
}

// funcion para listar los contactos

function listar_order($int){
	try{
		$conn = new PDO('sqlite:agendaphp.sqlite');
		if ($int == 1){
			$consulta = "SELECT * FROM contactos order by nombre";
		}
		if ($int == 2){
			$consulta = "SELECT * FROM contactos order by apellidos";
		}
		if ($int == 3){
			$consulta = "SELECT * FROM contactos order by telefono";
		}
		if ($int == 4){
			$consulta = "SELECT * FROM contactos order by email";
		}
		if ($int == 5){
			$consulta = "SELECT * FROM contactos order by nombre DESC";
		}
		if ($int == 6){
			$consulta = "SELECT * FROM contactos order by apellidos DESC";
		}
		if ($int == 7){
			$consulta = "SELECT * FROM contactos order by telefono DESC";
		}
		if ($int == 8){
			$consulta = "SELECT * FROM contactos order by email DESC";
		}
		
		$resultado = $conn -> query($consulta);

		echo "<table class='contactos'>
		<tr><th>Nombre<a href='orderNombre.php'><img class='flecha' src='img/flecha.jpg' alt='ordenar'><a href='orderNombre2.php'><img class='flecha' src='img/flecha2.jpg' alt='ordenar'></a></th>
		<th>Apellidos<a href='orderApe.php'><img class='flecha' src='img/flecha.jpg' alt='ordenar'></a><a href='orderApe2.php'><img class='flecha' src='img/flecha2.jpg' alt='ordenar'></th>
		<th>Telefono<a href='orderTelf.php'><img class='flecha' src='img/flecha.jpg' alt='ordenar'></a><a href='orderTelf2.php'><img class='flecha' src='img/flecha2.jpg' alt='ordenar'></th>
		<th>Email<a href='orderEmail.php'><img class='flecha' src='img/flecha.jpg' alt='ordenar'></a><a href='orderEmail2.php'><img class='flecha' src='img/flecha2.jpg' alt='ordenar'></th></tr>";
		foreach ($resultado as $contacto) {
			echo "<tr><td>", $contacto['nombre'],"</td>";
			echo "<td>", $contacto['apellidos'],"</td>";
			echo "<td>", $contacto['telefono'],"</td>";
			echo "<td>", $contacto['email'],"</td></tr>";
		}
		echo "</table>";

	}catch(PDOException $e ){
		echo $e -> getMessage();
	}
	$conn = null;
}

// funcion para borrar todos los registros de la tabla

function borrarTodo(){
	try{
		$con = new PDO('sqlite:agendaphp.sqlite');
		$consulta = "DELETE  FROM  contactos";
		$con ->exec($consulta);
		header("Location: inicio.php");
	}catch (PDOException $e){
		echo $e ->getMessage();
	}
}

//funcion para editar registro

function editar(){
	if ($_REQUEST){
		$nombre = $_POST['nombre'];
		$apellidos = $_POST['apellidos'];
		$email = $_POST['email'];
		$telefono = $_POST['telefono'];
	}
}

// funcion para añadir un registro

function insertar($nombre, $apellidos, $email, $telefono){
	if(isset($_POST['nombre'], $_POST['apellidos'], $_POST['email'], $_POST['telefono'])){
	    $nombre = $_POST['nombre'];
	    $apellidos = $_POST['apellidos'];
	    $email = $_POST['email'];
	    $telefono = $_POST['telefono'];
	   
	    
	    try{   
	            $conn = new PDO('sqlite:agendaphp.sqlite');
	            $insertar = "INSERT INTO contactos (nombre, apellidos, telefono, email) 
	            							VALUES ('" . $nombre ."', '" . $apellidos . "', '"  . $telefono . "', '" . $email . "')";
	            $sentencia=$conn->prepare($insertar);
	            if ($sentencia->execute()) {
	                echo "<p>Contacto añadido a la agenda.</p>";
	                
	            } 
	            else {
	                echo "<p>Error al añadir el contacto.</p>";
	            }
	                
	                       
	        }catch(PDOException $e ){
	            echo $e -> getMessage();
	        }
	        
	        $conn=null;
	        
	       
	}
}

// funcion para crear formulario con checkbox para marcar eliminar contacto

function marcaeliminar(){

	try{
		$conn = new PDO('sqlite:agendaphp.sqlite');
		$consulta = "SELECT * FROM contactos";
		$contactos = $conn -> query($consulta);

		echo "<form action='borrar.php' method='POST'><table>
		<tr><th>borrar</th><th>Nombre</th><th>Apellidos</th><th>Telefono</th><th>Email</td></tr>";
		foreach ($contactos as $contacto) {
			echo "<tr><td><input type='checkbox' value='".$contacto['id']."' name='borrar[]'/></td>";
			echo "<td>", $contacto['nombre'],"</td>";
			echo "<td>", $contacto['apellidos'],"</td>";
			echo "<td>", $contacto['telefono'],"</td>";
			echo "<td>", $contacto['email'],"</td></tr>";
		}
		echo "<tr><td><input type='submit' value='BORRAR'/></td></tr></table></form>";
		$conn = null;
	}catch(PDOException $e ){
		echo $e -> getMessage();
	}
}
// funcion para crear formulario con checkbox para marcar editar contacto

function marcaeditar(){

	try{
		$conn = new PDO('sqlite:agendaphp.sqlite');
		$consulta = "SELECT * FROM contactos";
		$resultado = $conn -> query($consulta);

		echo "<form method='GET' action='editar.php'><table class='contactos'>
		<tr><th>editar</th><th>Nombre</th><th>Apellidos</th><th>Telefono</th><th>Email</th></tr>";
		foreach ($resultado as $contacto) {
			echo "<tr><td><input type='radio' id=".$contacto['id']." value=".$contacto['id']." name='mod'</td>";
			echo "<td>", $contacto['nombre'],"</td>";
			echo "<td>", $contacto['apellidos'],"</td>";
			echo "<td>", $contacto['telefono'],"</td>";
			echo "<td>", $contacto['email'],"</td></tr>";
		}
		echo "<tr><td><input type='submit' value='editar'/></td></tr></table></form>";
		$conn = null;
	}catch(PDOException $e ){
		echo $e -> getMessage();
	}
}




// funcion para guardar cambios en contactos

function guardarCambios($id, $nombre, $apellidos, $telefono, $email){
	try{
		echo "entra";
		$conn = new PDO('sqlite:agendaphp.sqlite');

		$consulta = "UPDATE contactos SET nombre ='" .$nombre ."', apellidos = '" . $apellidos . "', telefono = '"  . $telefono . "', email = '" . $email . "'
		WHERE id = '" . $id . "'";
		echo $consulta;
		$conn -> execute($consulta);
		$conn = null;

	}catch(PDOException $e ){
		return $e -> getMessage();
	}
}

//funcion borrar registro

function borrarContacto(){
	if(isset($_POST['borrar'])){
	
		$contBorrar = $_POST['borrar'];
	
	
		if(count($contBorrar) < 1){
			echo "No hay ningún contacto seleccionado";
		}
	
		if(count($contBorrar) > 0){
				
			try{
	
				$conn = new PDO('sqlite:agendaphp.sqlite');
				for($i = 0; $i < count($contBorrar); $i++){
						
					$consulta = "DELETE  FROM contactos WHERE id=". $contBorrar[$i] . "";
					$conn->exec($consulta);
					echo "los contactos se han eliminado.";
					header("Location: borrar.php");
						
				}
				$conn =null;
	
			}catch(PDOException $e ){
				echo $e -> getMessage();
			}
		}
	}
}

?>